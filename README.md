# Tapuino Rewind #

This is a fork of Tapuino code from: https://github.com/sweetlilmre/tapuino

### Modifications ###

* Changed font and motor indicator (done by Marko Solajic)
* Removed **sprintf** function from the code, its usage replaced with less memory-hungry code, gaining about 10% of free FLASH memory
* Maximum LFN path shortened from 256 to 128 characters, to make room for position variables (could be increased back when the design is finished)
* Added code in pause menu to save/load current TAP position into one of 5 available slots. When in pause menu, press:
    * Left to choose one of pause options (load or save position)
    * and then Right to select option
    * Left to choose one of slots
    * and then Right to select the slot
* Changed display code to allow writing to 4 lines

### TODO ###

* Saving/loading of positions to SD card. If someone knows how to do this using Tapuino's FAT library, please contact me...
* Integrate changes in main Tapuino repository?
