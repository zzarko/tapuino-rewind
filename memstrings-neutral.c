#include <avr/pgmspace.h>
#include "memstrings.h"
#include "version.h"

const char S_NAME_PATTERN[] PROGMEM = "rec-0000.tap";
const char S_TAPUINO_VERSION[] PROGMEM = "Version "TAPUINO_MAJOR_VERSION"."TAPUINO_MINOR_VERSION"."TAPUINO_BUILD_VERSION;
const char S_FILENAME_CHARS[] PROGMEM = " abcdefghijklmnopqrstuvwxyz0123456789_-";

const char S_C64[] PROGMEM = "C64";
const char S_C16[] PROGMEM = "C16";
const char S_VIC[] PROGMEM = "VIC";
const char S_PAL[] PROGMEM = "PAL";
const char S_NTSC[] PROGMEM = "NTSC";
